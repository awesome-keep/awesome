FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > awesome.log'

COPY awesome .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' awesome
RUN bash ./docker.sh

RUN rm --force --recursive awesome
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD awesome
